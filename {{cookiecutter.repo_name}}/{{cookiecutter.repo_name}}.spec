# sitelib for noarch packages, sitearch for others (remove the unneeded one)
%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())")}
%{!?python_sitearch: %global python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib(1))")}

Name:           {{ cookiecutter.repo_name }}
Version:        {{ cookiecutter.version }}
Release:        1%{?dist}
Summary:        {{ cookiecutter.project_short_description }}

License:        {{ cookiecutter.license }}
URL:            https://{{ cookiecutter.hosting_site }}/{{ cookiecutter.hosting_usename }}/{{ cookiecutter.repo_name }}
Source0:        {{ cookiecutter.repo_name }}-{{ cookiecutter.version }}.tar.gz

BuildArch:      noarch

Requires:       python-flask
Requires:       python-flask-sqlalchemy
Requires:       python-flask-wtf
Requires:       python-flask-assets
Requires:       python-alembic
Requires:       python-cssmin
Requires:       uglify-js
BuildRequires:  python-devel
BuildRequires:  python-setuptools
BuildRequires:  pytest
BuildRequires:  python-flask
BuildRequires:  python-flask-sqlalchemy
BuildRequires:  python-fedora-flask
BuildRequires:  python-mock
BuildRequires:  python-dingus

# python-sqlalchemy is too old on el6 but required on fedora
# At the moment, there is no el6 build of python-dingus
%if 0%{?rhel}
Requires:       python-sqlalchemy0.7
BuildRequires:  python-sqlalchemy0.7
BuildRequires:  python-sphinx10
%else
Requires:       python-sqlalchemy
BuildRequires:  python-sqlalchemy
BuildRequires:  python-sphinx
%endif


%description
{{ cookiecutter.project_short_description }} needs a better description

%prep
%setup -q

# disabling check on el6
%if 0%{?rhel}
#%check
#%{__python} setup.py test
%else
%check
%{__python} setup.py test
%endif


%build
%{__python} setup.py build

# build docs using sphinx-1.0 on el6
%if 0%{?rhel}
sphinx-1.0-build  -b html -d docs/_build/doctrees docs/source docs/_build/html
sphinx-1.0-build  -b man -d docs/_build/doctrees docs/source docs/_build/man
%else
sphinx-build  -b html -d docs/_build/doctrees docs/source docs/_build/html
sphinx-build  -b man -d docs/_build/doctrees docs/source docs/_build/man
%endif


%install
rm -rf $RPM_BUILD_ROOT
%{__python} setup.py install -O1 --skip-build --root %{buildroot}

# alembic stuff
mkdir -p %{buildroot}%{_datadir}/{{ cookiecutter.repo_name }}
cp -r alembic %{buildroot}%{_datadir}/{{ cookiecutter.repo_name }}/.
cp alembic.ini %{buildroot}%{_datadir}/{{ cookiecutter.repo_name }}/.

# apache config and wsgi stuff
mkdir -p %{buildroot}%{_datadir}/{{ cookiecutter.repo_name }}/conf
cp conf/{{ cookiecutter.repo_name }}.conf %{buildroot}%{_datadir}/{{ cookiecutter.repo_name }}/conf/.
cp conf/{{ cookiecutter.repo_name }}.wsgi %{buildroot}%{_datadir}/{{ cookiecutter.repo_name }}/.

# configuration file
mkdir -p %{buildroot}%{_sysconfdir}/{{ cookiecutter.repo_name }}
install conf/settings.py.example %{buildroot}%{_sysconfdir}/{{ cookiecutter.repo_name }}/settings.py.example

# man page for CLI
mkdir -p %{buildroot}/%{_mandir}/man1/
cp -v docs/_build/man/*.1 %{buildroot}/%{_mandir}/man1/


%pre

# add a user and group mostly for running the cron job
%{_sbindir}/groupadd -r %{name} &>/dev/null || :
%{_sbindir}/useradd  -r -s /sbin/nologin -d %{_datadir}/%{name} -M \
                     -c 'Blocker Tracking App' -g %{name} %{name} &>/dev/null || :

%files
%doc docs/_build/html/*
%doc %{_mandir}/man1/{{ cookiecutter.repo_name }}*

%{python_sitelib}/*

%attr(755,root,root) %{_bindir}/{{ cookiecutter.repo_name }}
%dir %{_sysconfdir}/{{ cookiecutter.repo_name }}
%{_sysconfdir}/{{ cookiecutter.repo_name }}/*
%dir %{_datadir}/{{ cookiecutter.repo_name }}
%{_datadir}/{{ cookiecutter.repo_name }}/*


%changelog
* Sun Dec 1 2013  {{ cookiecutter.full_name }} <{{ cookiecutter.email }}> - {{ cookiecutter.version }}-1
- Initial package for {{ cookiecutter.project_name }}
