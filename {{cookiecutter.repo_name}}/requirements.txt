Flask==0.9
Flask-SQLAlchemy==0.16
Flask-WTF>=0.8
Jinja2==2.6
SQLAlchemy==0.7.8
WTAlchemy==0.1b3
WTForms>=1.0.1
Werkzeug==0.8.3
alembic==0.5.0

