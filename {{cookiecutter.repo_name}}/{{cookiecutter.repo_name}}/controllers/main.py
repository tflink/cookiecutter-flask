

from flask import Blueprint, render_template,

main = Blueprint('main', __name__)

@main.route('/')
def index():
    if app.debug:
        app.logger.debug('rendering index')
    return render_template('index.html', title='Fedora Blocker Bugs')

