=============================
cookiecutter-flask-foundation
=============================

Cookiecutter template for a Flask_ webapp. See https://.com/audreyr/cookiecutter.

* Free software: BSD license
* Vanilla testing setup with `py.test` and `python setup.py test`
* Tox_ testing: Setup to easily test for Python 2.6, 2.7, 3.3
* Sphinx_ docs: Documentation ready for generation with, for example, ReadTheDocs_
* Alembic_ schema control
* CSS generation using Compass_ and SASS_

This template is a fork of `audreyr's cookiecutter-pypackage
<https://github.com/audreyr/cookiecutter-pypackage/>`_ template and also includes
code inspired by `Fedora's Blocker Tracking App <https://git.fedorahosted.org/cgit/blockerbugs.git>`_
(relicensed with permission).

Usage
-----

Generate a Python package project::

    cookiecutter https://bitbucket.org/tflink/cookiecutter-flask-foundation.git

You will be prompted for information during the creation process or you can accept the defaults. Then:

* Create a repo and put it there.
* Add the repo to your ReadTheDocs account + turn on the ReadTheDocs service hook.
* Release your package the standard Python way. Here's a release checklist: https://gist.github.com/audreyr/5990987

Not Exactly What You Want?
--------------------------

Don't worry, you have options:

Similar Cookiecutter Templates
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* Also see the forks of  this repo. (If you find anything that should be listed
  here, please add it and send a pull request!)

Fork This / Create Your Own
~~~~~~~~~~~~~~~~~~~~~~~~~~~

If you have differences in your preferred setup, I encourage you to fork this
to create your own version. Or create your own; it doesn't strictly have to
be a fork.

* Once you have your own version working, add it to the Similar Cookiecutter
  Templates list above with a brief description. 

* It's up to you whether or not to rename your fork/own version. Do whatever
  you think sounds good.

Or Submit a Pull Request
~~~~~~~~~~~~~~~~~~~~~~~~

I also accept pull requests on this, if they're small, atomic, and if they
make my own packaging experience better.


.. _Flask: http://flask.pocoo.org/
.. _Alembic: https://bitbucket.org/zzzeek/alembic
.. _Compass: http://compass-style.org/
.. _SASS: http://sass-lang.com/
.. _Tox: http://testrun.org/tox/
.. _Sphinx: http://sphinx-doc.org/
.. _ReadTheDocs: https://readthedocs.org/
